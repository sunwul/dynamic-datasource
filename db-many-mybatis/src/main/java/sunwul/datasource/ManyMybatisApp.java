package sunwul.datasource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*****
 * @author sunwul
 * @date 2022/2/25 8:55
 * @description
 */
@SpringBootApplication
public class ManyMybatisApp {

    public static void main(String[] args) {
        SpringApplication.run(ManyMybatisApp.class, args);
    }
}
