package sunwul.datasource.mapper.write;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/*****
 * @author sunwul
 * @date 2022/2/14 19:36
 * @description
 */
@Component("TAdminMapper_W")
public interface TAdminMapper_manyMybatis {

    Integer add_admin(@Param("name") String name);
}
