package sunwul.datasource.mapper.read;

import org.springframework.stereotype.Component;
import sunwul.datasource.entity.TAdmin_manyMybatis;

import java.util.List;

/*****
 * @author sunwul
 * @date 2022/2/14 19:36
 * @description
 */
@Component("TAdminMapper_R")
public interface TAdminMapper_manyMybatis {

    List<TAdmin_manyMybatis> sel_admin();

    Integer sel_admin_id();

}
