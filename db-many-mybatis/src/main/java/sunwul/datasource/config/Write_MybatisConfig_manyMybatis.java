package sunwul.datasource.config;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import sunwul.datasource.common.FactoryUtil_manyMybatis;

import javax.sql.DataSource;

/*****
 * @author sunwul
 * @date 2022/2/25 11:27
 * @description
 */
@Configuration
@MapperScan(basePackages = "sunwul.datasource.mapper.write", sqlSessionFactoryRef = "sqlSessionFactory_w")
public class Write_MybatisConfig_manyMybatis {

    /**
     * 写库的数据源
     *
     * @return DataSource
     */
    @Bean("dataSource_write")
    @ConfigurationProperties(prefix = "spring.datasource.datasource-write")
    public DataSource dataSource_write() {
        System.out.println("==============读取配置信息,创建DataSource=============");
        // 通过绑定全局配置,拿到属性后自动创建一个DruidDataSource
        return DruidDataSourceBuilder.create().build();
    }

    /**
     * 配置写库的 SqlSessionFactory
     *
     * @return SqlSessionFactory
     * @throws Exception ex
     */
    @Bean
    @Primary
    public SqlSessionFactory sqlSessionFactory_w() throws Exception {
        return FactoryUtil_manyMybatis.createFactory(dataSource_write(), "classpath:mapper/write/*Mapper_manyMybatis.xml", true);
    }

}
