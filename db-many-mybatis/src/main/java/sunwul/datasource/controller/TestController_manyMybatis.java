package sunwul.datasource.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sunwul.datasource.entity.TAdmin_manyMybatis;

import java.util.List;
import java.util.stream.Collectors;

/*****
 * @author sunwul
 * @date 2022/2/28 9:33
 * @description
 */
@RestController
public class TestController_manyMybatis {

    @Autowired
//    @Qualifier("TAdminMapper_R")
    sunwul.datasource.mapper.read.TAdminMapper_manyMybatis mapper_r;

    @Autowired
//    @Qualifier("TAdminMapper_W")
    sunwul.datasource.mapper.write.TAdminMapper_manyMybatis mapper_w;

    @RequestMapping("/sel")
    public List<TAdmin_manyMybatis> toSel(){
        return mapper_r.sel_admin();
    }

    @RequestMapping("/selOne/{id}")
    public TAdmin_manyMybatis toSelOne(@PathVariable("id") Integer id){
        return mapper_r.sel_admin().stream().filter(s -> s.getId() == id).distinct().collect(Collectors.toList()).get(0);
    }

    @RequestMapping("/add/{name}")
    public Integer toAdd(@PathVariable("name") String name){
        return mapper_w.add_admin(name);
    }
}
