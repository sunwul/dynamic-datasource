package sunwul.datasource.mapper.read;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;
import sunwul.datasource.entity.TAdmin_springTran;

import java.util.List;

/*****
 * @author sunwul
 * @date 2022/2/14 19:36
 * @description
 */
@Component("TAdminMapper_R")
@Mapper
public interface TAdminMapper_springTran {

    List<TAdmin_springTran> sel_admin();

    Integer add_admin(@Param("name") String name);

}
