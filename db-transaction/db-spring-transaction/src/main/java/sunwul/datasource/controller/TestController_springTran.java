package sunwul.datasource.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sunwul.datasource.entity.TAdmin_springTran;
import sunwul.datasource.service.TAdminService_springTran;

import java.util.List;
import java.util.stream.Collectors;

/*****
 * @author sunwul
 * @date 2022/2/28 9:33
 * @description
 */
@RestController
public class TestController_springTran {

    @Autowired
    TAdminService_springTran tAdminService;

    @RequestMapping("/sel")
    public List<TAdmin_springTran> toSel(){
        return tAdminService.sel_admin();
    }

    @RequestMapping("/selOne/{id}")
    public TAdmin_springTran toSelOne(@PathVariable("id") Integer id){
        return tAdminService.sel_admin().stream().filter(s -> s.getId() == id).distinct().collect(Collectors.toList()).get(0);
    }

    @RequestMapping("/add/{name}")
    public Integer toAdd(@PathVariable("name") String name){
        return tAdminService.add_admin(name);
    }
}
