package sunwul.datasource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author sunwul
 * @date 2022/3/8 22:57:51
 * @description
 */
@SpringBootApplication
@EnableTransactionManagement // 开启事务
public class SpringTransactionApp {

    public static void main(String[] args) {
        SpringApplication.run(SpringTransactionApp.class, args);
    }
}
