package sunwul.datasource.service;

import org.apache.ibatis.annotations.Param;
import sunwul.datasource.entity.TAdmin_springTran;

import java.util.List;

/**
 * @author sunwul
 * @date 2022/3/10 22:24:42
 * @description
 */
public interface TAdminService_springTran {

    List<TAdmin_springTran> sel_admin();

    Integer add_admin(@Param("name") String name);

}
