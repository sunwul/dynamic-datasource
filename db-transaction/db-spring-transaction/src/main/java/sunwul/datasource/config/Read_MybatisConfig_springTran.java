package sunwul.datasource.config;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;
import sunwul.datasource.common.FactoryUtil_springTran;

import javax.sql.DataSource;

/*****
 * @author sunwul
 * @date 2022/2/25 9:22
 * @description
 * * 指定扫描的包
 * * 指定使用的sqlSessionFactory
 */
@Configuration
@MapperScan(basePackages = "sunwul.datasource.mapper.read", sqlSessionFactoryRef = "sqlSessionFactory_r")
public class Read_MybatisConfig_springTran {

    /**
     * 读库的数据源
     *
     * @return DataSource
     */
    @Bean("dataSource_read")
    @ConfigurationProperties(prefix = "spring.datasource.datasource-read")
    public DataSource dataSource_read() {
        System.out.println("==============读取配置信息,创建DataSource=============");
        // 通过绑定全局配置,拿到属性后自动创建一个DruidDataSource
        return DruidDataSourceBuilder.create().build();
    }

    /**
     * 配置读库的 SqlSessionFactory
     *
     * @return SqlSessionFactory
     * @throws Exception ex
     */
    @Bean
    public SqlSessionFactory sqlSessionFactory_r() throws Exception {
        return FactoryUtil_springTran.createFactory(dataSource_read(), "classpath:mapper/read/*Mapper_springTran.xml", true);
    }

    /**
     * 读库的事务管理
     *
     * @return DataSourceTransactionManager
     */
    @Bean
    public DataSourceTransactionManager dataSourceTransactionManager_r() {
        DataSourceTransactionManager dataSourceTransactionManager = new DataSourceTransactionManager();
        dataSourceTransactionManager.setDataSource(dataSource_read());
        return dataSourceTransactionManager;
    }

    /**
     * 事务模板  -  Spring编程式事务
     *
     * @return TransactionTemplate
     */
    @Bean
    public TransactionTemplate transactionTemplate_r() {
        return new TransactionTemplate(dataSourceTransactionManager_r());
    }

}
