/*
 Navicat Premium Data Transfer

 Source Server         : localhost_mysql(ubuntu)
 Source Server Type    : MySQL
 Source Server Version : 80028
 Source Host           : localhost:3306
 Source Schema         : write

 Target Server Type    : MySQL
 Target Server Version : 80028
 File Encoding         : 65001

 Date: 18/07/2023 23:11:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_admin
-- ----------------------------
DROP TABLE IF EXISTS `t_admin`;
CREATE TABLE `t_admin`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `phone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `telephone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `enabled` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `user_face` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_admin
-- ----------------------------
INSERT INTO `t_admin` VALUES (25, 'test001', '13800000000', '13800000000', '江苏省无锡市梁溪区晶品公寓', '1', 'test4', '$2a$10$RUNJL76af9fDKJM12SITGu8tnrl6vggvK1nCHOj0.phrksn94oCby', 'https://ae01.alicdn.com/kf/Uf813349d87714b2e90925a7f0ba744f0h.jpg', '人事部');
INSERT INTO `t_admin` VALUES (28, 'test002', '13800000000', '13800000000', '江苏省无锡市梁溪区晶品公寓', '1', 'test4', '$2a$10$RUNJL76af9fDKJM12SITGu8tnrl6vggvK1nCHOj0.phrksn94oCby', 'https://ae01.alicdn.com/kf/Uf813349d87714b2e90925a7f0ba744f0h.jpg', '人事部');
INSERT INTO `t_admin` VALUES (30, 'sunwul', '13800000000', '13800000000', '江苏省无锡市梁溪区晶品公寓', '1', 'test4', '$2a$10$RUNJL76af9fDKJM12SITGu8tnrl6vggvK1nCHOj0.phrksn94oCby', 'https://ae01.alicdn.com/kf/Uf813349d87714b2e90925a7f0ba744f0h.jpg', '人事部');
INSERT INTO `t_admin` VALUES (31, 'list', '13800000000', '13800000000', '江苏省无锡市梁溪区晶品公寓', '1', 'test4', '$2a$10$RUNJL76af9fDKJM12SITGu8tnrl6vggvK1nCHOj0.phrksn94oCby', 'https://ae01.alicdn.com/kf/Uf813349d87714b2e90925a7f0ba744f0h.jpg', '人事部');
INSERT INTO `t_admin` VALUES (33, 'sunwul', '13800000000', '13800000000', '江苏省无锡市梁溪区晶品公寓', '1', 'test4', '$2a$10$RUNJL76af9fDKJM12SITGu8tnrl6vggvK1nCHOj0.phrksn94oCby', 'https://ae01.alicdn.com/kf/Uf813349d87714b2e90925a7f0ba744f0h.jpg', '人事部');

SET FOREIGN_KEY_CHECKS = 1;
