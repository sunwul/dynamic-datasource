package sunwul.datasource.config;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.sql.SQLException;

/*****
 * @author sunwul
 * @date 2022/2/14 19:32
 * @description  数据源配置
 */
@Configuration
public class DatasourceConfig {

    @Bean("dataSource_write")
    @ConfigurationProperties(prefix = "spring.datasource.datasource-write")
    public DataSource dataSource_write() {
        System.out.println("==============读取配置信息,创建DataSource=============");
        // 通过绑定全局配置,拿到属性后自动创建一个DruidDataSource
        return DruidDataSourceBuilder.create().build();
    }

    @Bean("dataSource_read")
    @ConfigurationProperties(prefix = "spring.datasource.datasource-read")
    public DataSource dataSource_read() throws SQLException {
        System.out.println("==============读取配置信息,创建DataSource=============");
        // 通过绑定全局配置,拿到属性后自动创建一个DruidDataSource
        return DruidDataSourceBuilder.create().build();
    }
}
