package sunwul.datasource.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sunwul.datasource.entity.TAdmin;
import sunwul.datasource.mapper.TAdminMapper;
import sunwul.datasource.service.TAdminService;

import java.util.List;

/*****
 * @author sunwul
 * @date 2022/2/14 19:33
 * @description
 */
@Service("tAdminServiceImpl")
public class TAdminServiceImpl implements TAdminService {

    @Autowired
    TAdminMapper tAdminMapper;

    @Override
    public List<TAdmin> sel_admin() {
        return tAdminMapper.sel_admin();
    }

    @Override
    public Integer add_admin(String name) {
        return tAdminMapper.add_admin(name);
    }
}
