package sunwul.datasource.service;

import org.springframework.stereotype.Component;
import sunwul.datasource.entity.TAdmin;

import java.util.List;

/*****
 * @author sunwul
 * @date 2022/2/14 19:33
 * @description
 */
@Component
public interface TAdminService {

    /**
     * 查询数据
     * @return list
     */
    List<TAdmin> sel_admin();

    Integer add_admin(String name);
}
