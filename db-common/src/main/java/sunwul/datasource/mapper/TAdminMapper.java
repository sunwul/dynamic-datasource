package sunwul.datasource.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;
import sunwul.datasource.entity.TAdmin;

import java.util.List;

/*****
 * @author sunwul
 * @date 2022/2/14 19:36
 * @description
 */
@Component
public interface TAdminMapper {

    List<TAdmin> sel_admin();

    Integer sel_admin_id();

    Integer add_admin(@Param("name") String name);
}
