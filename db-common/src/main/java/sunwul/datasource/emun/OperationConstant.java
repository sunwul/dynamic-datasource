package sunwul.datasource.emun;

/*****
 * @author sunwul
 * @date 2022/2/23 9:16
 * @description 操作类型常量
 */
public class OperationConstant {

    // 读
    public static final String OPERATION_TYPE_R = "R";
    // 写
    public static final String OPERATION_TYPE_W = "W";
}
