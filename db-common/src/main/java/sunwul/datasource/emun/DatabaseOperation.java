package sunwul.datasource.emun;

/*****
 * @author sunwul
 * @date 2022/2/15 9:53
 * @description 数据库操作类型
 */
public enum DatabaseOperation {

    Read(OperationConstant.OPERATION_TYPE_R, "读取"),
    Write(OperationConstant.OPERATION_TYPE_W, "写入");

    private String name;
    private String desc;

    DatabaseOperation(String name, String desc) {
        this.name = name;
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
