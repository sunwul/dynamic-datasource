package sunwul.datasource.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.dynamic.datasource.annotation.DSTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sunwul.datasource.entity.TAdmin;
import sunwul.datasource.mapper.TestMapper;
import sunwul.datasource.service.TestService;

import java.util.List;

/**
 * @author sunwul
 * @date 2022/3/19 22:08:35
 * @description
 */
@Service
public class TestServiceImpl implements TestService {

    @Autowired
    TestMapper testMapper;

    @Override
    @DS("master") // 数据源
    public void save(TAdmin admin) {
        testMapper.save(admin);
    }

    @Override
    @DS("salve") // 从库如果按照下划线命名方式配置多个,指定前缀即可(有一个分组的概念)
    public List<TAdmin> list() {
        return testMapper.list();
    }

    @Override
    @DS("master")
    @DSTransactional // 框架提供的事务控制
    public void saveAll(){
        TAdmin admin = new TAdmin();
        admin.setName("sunwul");
        testMapper.save(admin);
        testMapper.list();
//        int a = 1/0;
    }
}
