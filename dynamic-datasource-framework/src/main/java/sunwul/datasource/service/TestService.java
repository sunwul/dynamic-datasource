package sunwul.datasource.service;

import sunwul.datasource.entity.TAdmin;

import java.util.List;

/**
 * @author sunwul
 * @date 2022/3/19 22:08:21
 * @description
 */
public interface TestService {

    void save(TAdmin admin);

    List<TAdmin> list();

    void saveAll();
}
