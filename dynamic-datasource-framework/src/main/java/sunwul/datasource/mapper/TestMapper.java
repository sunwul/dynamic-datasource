package sunwul.datasource.mapper;

import org.apache.ibatis.annotations.Param;
import sunwul.datasource.entity.TAdmin;

import java.util.List;

/**
 * @author sunwul
 * @date 2022/3/19 22:09:05
 * @description
 */
public interface TestMapper {

    void save(@Param("admin") TAdmin admin);

    List<TAdmin> list();

}
