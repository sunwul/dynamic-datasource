package sunwul.datasource;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author sunwul
 * @date 2022/3/19 22:07:10
 * @description
 */
@SpringBootApplication
@MapperScan(basePackages = "sunwul.datasource.mapper")
public class FrameWorkApp {

    public static void main(String[] args) {
        SpringApplication.run(FrameWorkApp.class, args);
    }
}
