package sunwul.datasource.controller;

import com.baomidou.dynamic.datasource.DynamicRoutingDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import sunwul.datasource.entity.TAdmin;
import sunwul.datasource.service.TestService;

import javax.sql.DataSource;
import java.util.List;

/**
 * @author sunwul
 * @date 2022/3/19 22:07:53
 * @description
 */
@RestController
public class TestController {

    @Autowired
    TestService testService;

    @GetMapping("/list")
    public List<TAdmin> list(){
        return testService.list();
    }

    @GetMapping("/save/{name}")
    public void save(@PathVariable("name") String name){
        TAdmin admin = new TAdmin();
        admin.setName(name);
        testService.save(admin);
    }

    @GetMapping("/all")
    public void all(){
        testService.saveAll();
    }

    @Autowired
    DataSource datsSource;

    // 删除数据源
    @DeleteMapping("/del")
    public String remove(){
        DynamicRoutingDataSource ds = (DynamicRoutingDataSource) datsSource;
        ds.removeDataSource("salve_1");
        return "删除成功";
    }
}
