package sunwul.datasource.db;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import sunwul.datasource.emun.DatabaseOperation;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

/*****
 * @author sunwul
 * @date 2022/2/21 15:32
 * @description 动态数据源
 * 实现DataSource
 *      - 重写getConnection()方法, 自定义逻辑获取数据源连接
 * 实现InitializingBean:
 *      - spring提供的一个用来初始化的回调接口,凡是实现该接口的类，
 *      - 在初始化bean的时候都会执行afterPropertiesSet()方法
 *
 * 此方法不成熟,不稳定, 其中许多方法未定义实现逻辑, 全部实现逻辑则又太麻烦
 * 当Spring框架需要调用到其中某个方法时, 由于方法重写但未实现自定义逻辑,会出错
 *      - 解决方法: 查看模块 db-extends-abstractroutingdatasource中的DynamicDatasource类,
 */
@Component
@Primary  // 作为主要注入Bean, 当出现相同类型的Bean时,会使用含此注解的Bean
public class DynamicDatasource implements DataSource, InitializingBean {

    // 当前使用的数据源标识  线程安全(多线程对这个name进行同时读写时,会出现单独或篡改的情况)
    public static ThreadLocal<String> name = new ThreadLocal<>();

    // 写
    @Autowired
    DataSource dataSource_write;

    // 读
    @Autowired
    DataSource dataSource_read;

    // 实现DataSource 重写获取连接的方法
    @Override
    public Connection getConnection() throws SQLException {
        if (name.get().equals(DatabaseOperation.Read.getName())) {
            return dataSource_read.getConnection();
        }
        if (name.get().equals(DatabaseOperation.Write.getName())) {
            return dataSource_write.getConnection();
        }
        throw new RuntimeException("未获取到数据源连接!");
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        return null;
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return false;
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        return null;
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {

    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {

    }

    @Override
    public int getLoginTimeout() throws SQLException {
        return 0;
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return null;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        // TODO 初始化...
        // 默认的数据源标识是 W 写
        name.set(DatabaseOperation.Write.getName());
    }
}
