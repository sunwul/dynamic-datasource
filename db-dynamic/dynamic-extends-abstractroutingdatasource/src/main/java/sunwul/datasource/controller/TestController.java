package sunwul.datasource.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sunwul.datasource.db.DynamicDatasource;
import sunwul.datasource.emun.DatabaseOperation;
import sunwul.datasource.entity.TAdmin;
import sunwul.datasource.service.TAdminService;

import java.util.List;

/*****
 * @author sunwul
 * @date 2022/2/21 16:13
 * @description
 */
@RestController
public class TestController {

    @Autowired
    TAdminService tAdminServiceImpl;

    @RequestMapping("/sel")
    public List<TAdmin> sel_admin() {
        // 切换数据源标识
        DynamicDatasource.name.set(DatabaseOperation.Read.getName());
        return tAdminServiceImpl.sel_admin();
    }

    @RequestMapping("/selOne")
    public TAdmin selOne_admin() {
        DynamicDatasource.name.set(DatabaseOperation.Read.getName());
        return tAdminServiceImpl.sel_admin().get(0);
    }

    @RequestMapping("/add/{name}")
    public Integer add_admin(@PathVariable String name) {
        DynamicDatasource.name.set(DatabaseOperation.Write.getName());
        return tAdminServiceImpl.add_admin(name);
    }
}
