package sunwul.datasource;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*****
 * @author sunwul
 * @date 2022/2/21 16:25
 * @description
 */
@SpringBootApplication
@MapperScan(basePackages = "sunwul.datasource.mapper")
public class ExtendAbstractRoutingDataSourceApp {

    public static void main(String[] args) {
        SpringApplication.run(ExtendAbstractRoutingDataSourceApp.class, args);
    }
}
