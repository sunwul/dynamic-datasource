package sunwul.datasource;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*****
 * @author sunwul
 * @date 2022/2/21 20:52
 * @description
 */
@SpringBootApplication
@MapperScan(basePackages = "sunwul.datasource.mapper")
public class SwitchMybatisApp {

    public static void main(String[] args) {
        SpringApplication.run(SwitchMybatisApp.class, args);
    }
}
