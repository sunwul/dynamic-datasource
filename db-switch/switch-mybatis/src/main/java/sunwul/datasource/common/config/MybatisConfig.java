package sunwul.datasource.common.config;

import org.apache.ibatis.plugin.Interceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import sunwul.datasource.common.plugin.DynamicDataSourcePlugin;

/*****
 * @author sunwul
 * @date 2022/2/21 21:53
 * @description
 */
@Configuration
public class MybatisConfig {

    /**
     * 自定义的Bean会在MybatisAutoConfiguration实例化前创建完成
     * 在MybatisAutoConfiguration实例化时,会自动扫描并注入所用到的所有Bean,此时就注入了我们自定义的Bean
     */
    @Bean
    public Interceptor dynamicDataSourcePlugin() {
        System.out.println("================创建拦截器=============");
        return new DynamicDataSourcePlugin();
    }
}
