package sunwul.datasource;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/*****
 * @author sunwul
 * @date 2022/2/21 21:57
 * @description
 */
@SpringBootApplication
@MapperScan(basePackages = "sunwul.datasource.mapper")
@EnableAspectJAutoProxy // 启用AOP支持
public class SwitchAopApp {

    public static void main(String[] args) {
        SpringApplication.run(SwitchAopApp.class, args);
    }
}
