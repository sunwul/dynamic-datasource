package sunwul.datasource.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sunwul.datasource.annotaion.DataSource;
import sunwul.datasource.emun.OperationConstant;
import sunwul.datasource.entity.TAdmin;
import sunwul.datasource.service.TAdminService;

import java.util.List;

/*****
 * @author sunwul
 * @date 2022/2/21 21:58
 * @description
 */
@RestController
public class TestController {

    @Autowired
    TAdminService tAdminServiceImpl;

    @DataSource(OperationConstant.OPERATION_TYPE_R)
    @RequestMapping("/sel")
    public List<TAdmin> sel_admin() {
        // 切换数据源标识 - 不在需要手动切换数据源, 使用AOP拦截特定注解及属性切换数据源
//        DynamicDatasource.name.set(DatabaseOperation.Read.getName());
        return tAdminServiceImpl.sel_admin();
    }

    @DataSource(OperationConstant.OPERATION_TYPE_R)
    @RequestMapping("/selOne")
    public TAdmin selOne_admin() {
//        DynamicDatasource.name.set(DatabaseOperation.Read.getName());
        return tAdminServiceImpl.sel_admin().get(0);
    }

    @DataSource(OperationConstant.OPERATION_TYPE_W)
    @RequestMapping("/add/{name}")
    public Integer add_admin(@PathVariable String name) {
//        DynamicDatasource.name.set(DatabaseOperation.Write.getName());
        return tAdminServiceImpl.add_admin(name);
    }
}
