package sunwul.datasource.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;
import sunwul.datasource.annotaion.DataSource;
import sunwul.datasource.db.DynamicDatasource;

/*****
 * @author sunwul
 * @date 2022/2/23 9:23
 * @description  AOP
 * * 不管声明在方法还是类上, 有一个核心问题, 切换数据源的时机必须要在数据库操作前
 * * 由此可知, 只能使用前置通知或者环绕通知
 */
@Component
@Aspect
public class DynamicDataSourceAspect {

    // 前置
    @Before("within(sunwul.datasource.controller.*) && @annotation(dataSource)")
    public void before(JoinPoint point, DataSource dataSource){
        String value = dataSource.value();
        DynamicDatasource.name.set(value);
        System.out.println("注解获取的数据源标识: " + value);
    }
}
