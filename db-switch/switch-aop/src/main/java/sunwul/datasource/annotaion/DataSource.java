package sunwul.datasource.annotaion;

import sunwul.datasource.emun.OperationConstant;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/*****
 * @author sunwul
 * @date 2022/2/22 17:11
 * @description  自定义注解  数据源注解
 * * @Target 注解目标,指定注解可以声明在方法与类上
 * * @Retention 保留方式,设置为保留策略中的"运行时",这样就可以通过反射来判断注解是否存在
 */
@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface DataSource {

    // 用于保存数据源标识, 提供给动态数据源使用  默认写库
    String value() default OperationConstant.OPERATION_TYPE_W;

}
